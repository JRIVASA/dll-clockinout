VERSION 5.00
Begin VB.Form FormClockInOutSelecter 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2805
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   7005
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2805
   ScaleWidth      =   7005
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameClockOut 
      Appearance      =   0  'Flat
      BackColor       =   &H009C9C9C&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   735
      Left            =   3960
      TabIndex        =   8
      Top             =   1800
      Width           =   1695
      Begin VB.Label ClockOut 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Clock In"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame FrameClockIn 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   735
      Left            =   1200
      TabIndex        =   6
      Top             =   1800
      Width           =   1695
      Begin VB.Label ClockIn 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Clock In"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7080
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   6360
         Picture         =   "FormClockInOutSelecter.frx":0000
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   3135
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4275
         TabIndex        =   4
         Top             =   105
         Width           =   1815
      End
   End
   Begin VB.Frame frame_buscarEmpleado 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Visible         =   0   'False
      Width           =   2175
      Begin VB.Label lbl_buscarEmpleado 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Buscar Empleado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   1935
      End
   End
   Begin VB.TextBox txt_NombreEmpleado 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   960
      Visible         =   0   'False
      Width           =   4335
   End
End
Attribute VB_Name = "FormClockInOutSelecter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Correlativo As String
Public isSupervisor As Boolean
Private MinDate As Date

Private Const DisabledColor = &H9C9C9C
Private Const EnabledColor = &HAE5B00

Private Sub ClockIn_Click()
    
    If FrameClockIn.BackColor = DisabledColor Then Exit Sub
    
    Dim Fecha As Date
    
    If isSupervisor Then
        Fecha = Interfaz_SeleccionDeFecha(Full)
        If CDbl(Fecha) = 0 Then Exit Sub
    End If
    
    Dim rsMarcaje As New ADODB.Recordset
    Dim sql As String
    
    On Error GoTo Errores
    
    sql = "select * from tr_empleados_marcajes where 1=2"
    
    VAD10Connection.BeginTrans
    
    rsMarcaje.Open sql, VAD10Connection, adOpenKeyset, adLockOptimistic, adCmdText
    
    With rsMarcaje
        .AddNew
            !idmarcaje = GetCorrelative("EMPLEADOS_MARCAJES", True)
            !CodigoEmpleado = Correlativo
            If isSupervisor Then !ClockInDate = FechaBD(Fecha, FBD_FULL, True)
        .Update
    End With
    
    VAD10Connection.Execute ("update ma_empleados set marcajeestatus=1 where c_codigo = '" & Correlativo & "'")
    
    Unload Me
    
    VAD10Connection.CommitTrans
    
    Exit Sub
    
Errores:
    
    MsgBox Err.Description
    VAD10Connection.RollbackTrans
    
End Sub

Private Sub ClockOut_Click()

    If FrameClockOut.BackColor = DisabledColor Then Exit Sub
    
    Dim FechaDate As Date
    Dim FechaDbl As String
    
    If isSupervisor Then
        FechaDate = Interfaz_SeleccionDeFecha(Full, MinDate)
        FechaDbl = FechaBD(FechaDate, FBD_FULL, False)
        If CDbl(FechaDate) = 0 Then Exit Sub
    Else
        FechaDbl = FechaBD(Now, FBD_FULL, False)
    End If
    
    Dim rsMarcaje As New ADODB.Recordset
    Dim sql As String
    
    sql = "select * from (SELECT *, cast('" & FechaDbl & "' as datetime) as ClockOutDateU FROM TR_EMPLEADOS_MARCAJES WHERE CODIGOEMPLEADO='" & Correlativo & "' and ClockOutDate is null) as tb1"
    
    On Error GoTo Errores
    
    VAD10Connection.BeginTrans
    rsMarcaje.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsMarcaje.EOF Then
        If CInt(Fix(DateDiff("n", rsMarcaje!ClockInDate, rsMarcaje!ClockOutDateU) / 60)) <= 12 Or isSupervisor Then
            Dim rsMarcajeUpdate As New ADODB.Recordset
            sql = "select * from tr_empleados_marcajes where codigoempleado = '" & Correlativo & "' and clockoutdate is null"
            rsMarcajeUpdate.Open sql, VAD10Connection, adOpenKeyset, adLockOptimistic, adCmdText
            With rsMarcajeUpdate
                !ClockOutDate = rsMarcaje!ClockOutDateU
                '!Horas = DateDiff("h", rsMarcaje!ClockInDate, rsMarcaje!ClockOutDateU)
                !Horas = Fix(DateDiff("n", rsMarcaje!ClockInDate, rsMarcaje!ClockOutDateU) / 60)
                '!Minutos = DateDiff("n", rsMarcaje!ClockInDate, rsMarcaje!ClockOutDateU)
                !Minutos = FormatNumber(((DateDiff("n", rsMarcaje!ClockInDate, rsMarcaje!ClockOutDateU) / 60) - CInt(!Horas)) * 60, 0)
                .Update
            End With
            VAD10Connection.Execute ("update ma_empleados set marcajeestatus=0 where c_codigo = '" & Correlativo & "'")
        Else
            MsgBox "Ha alcanzado el tiempo maximo para hacer Clock Out, comuniquese con su supervisor."
        End If
    End If
    
    VAD10Connection.CommitTrans
    
    Unload Me
    
    Exit Sub
    
Errores:
    
    MsgBox Err.Description
    VAD10Connection.RollbackTrans
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    lbl_buscarEmpleado.Caption = Stellar_Mensaje(109)
    ClockIn.Caption = Stellar_Mensaje(110)
    ClockOut.Caption = Stellar_Mensaje(111)
    
    If Not isSupervisor Then
        Dim rsMarcaje As New ADODB.Recordset
        Dim sql As String
        
        sql = "SELECT * FROM MA_EMPLEADOS WHERE c_Codigo = '" & Correlativo & "' AND MarcajeEstatus = '1'"
        
        rsMarcaje.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        FrameClockIn.Top = FrameClockIn.Top - 840
        FrameClockOut.Top = FrameClockOut.Top - 840
        
        Me.Height = Me.Height - 800
        
        If Not rsMarcaje.EOF Then
            FrameClockOut.BackColor = EnabledColor
            FrameClockIn.BackColor = DisabledColor
        Else
            FrameClockIn.BackColor = EnabledColor
            FrameClockOut.BackColor = DisabledColor
        End If
    Else
        frame_buscarEmpleado.Visible = True
        txt_NombreEmpleado.Visible = True
        FrameClockIn.BackColor = DisabledColor
        FrameClockOut.BackColor = DisabledColor
    End If
    
End Sub

Private Sub FrameClockIn_Click()
    ClockIn_Click
End Sub

Private Sub FrameClockOut_Click()
    ClockOut_Click
End Sub

Private Sub lbl_buscarEmpleado_Click()
    
    Dim rsEmpleado As New ADODB.Recordset
    Dim rsMarcajes As New ADODB.Recordset
    
    Dim sql As String
    
    Correlativo = BuscarInfo_Empleado(VAD10Connection, "", True)
    sql = "select * from ma_empleados where c_codigo = '" & Correlativo & "'"
    
    On Error GoTo Errores
    
    VAD10Connection.BeginTrans
    
    rsEmpleado.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEmpleado.EOF Then
        txt_NombreEmpleado.Text = rsEmpleado!cu_nombres & " " & rsEmpleado!cu_apellidos
        If CBool(rsEmpleado!marcajeestatus) Then
            FrameClockOut.BackColor = EnabledColor
            FrameClockIn.BackColor = DisabledColor
            sql = "select * from tr_empleados_marcajes where codigoempleado = '" & Correlativo & "' and clockoutdate is null"
            rsMarcajes.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
            If Not rsMarcajes.EOF Then
                MinDate = rsMarcajes!ClockInDate
            End If
        Else
            FrameClockIn.BackColor = EnabledColor
            FrameClockOut.BackColor = DisabledColor
        End If
    End If
    
    VAD10Connection.CommitTrans
    
    Exit Sub
    
Errores:
    
    VAD10Connection.RollbackTrans
    
End Sub
