VERSION 5.00
Begin VB.Form FormClockInOut 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11040
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   11040
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Clock In Out Supervisor"
      Height          =   1695
      Left            =   6480
      TabIndex        =   1
      Top             =   600
      Width           =   3495
   End
   Begin VB.CommandButton command1 
      Caption         =   "Clock In Out"
      Height          =   1695
      Left            =   600
      TabIndex        =   0
      Top             =   600
      Width           =   3495
   End
End
Attribute VB_Name = "FormClockInOut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub command1_Click()
    FormLogin.Show vbModal
    Dim Correlativo As String
    Dim Relacion As String
    If FormLogin.Logged Then
        Correlativo = FormLogin.Correlativo
    End If
    Unload FormLogin
    Set FormLogin = Nothing
    If Correlativo <> "" Then
        FormClockInOutSelecter.Correlativo = Correlativo
        FormClockInOutSelecter.Show vbModal
        Set FormClockInOutSelecter = Nothing
    End If
End Sub

Private Sub Command2_Click()
    FormClockInOutSelecter.isSupervisor = True
    FormClockInOutSelecter.Show vbModal
End Sub
