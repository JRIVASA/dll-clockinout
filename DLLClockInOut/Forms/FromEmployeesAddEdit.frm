VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form FormEmployeesAddEdit 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleMode       =   0  'User
   ScaleWidth      =   15420.53
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrameNotas 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   120
      TabIndex        =   51
      Top             =   8400
      Width           =   15075
      Begin VB.TextBox txt_notas 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   480
         TabIndex        =   19
         Top             =   600
         Width           =   14055
      End
      Begin VB.Label lbl_Notas 
         BackStyle       =   0  'Transparent
         Caption         =   "Notas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   52
         Top             =   120
         Width           =   615
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   735
         X2              =   15120
         Y1              =   300
         Y2              =   300
      End
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4095
      Left            =   12480
      TabIndex        =   47
      Top             =   2280
      Width           =   2724
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   1000
         MousePointer    =   99  'Custom
         Picture         =   "FromEmployeesAddEdit.frx":0000
         Stretch         =   -1  'True
         Top             =   120
         Width           =   600
      End
      Begin VB.Image Imagen 
         Appearance      =   0  'Flat
         Height          =   2715
         Left            =   75
         Stretch         =   -1  'True
         Top             =   1035
         Width           =   2505
      End
      Begin VB.Label lbl_Imagen_Titulo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Imagen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   375
         Left            =   810
         TabIndex        =   49
         Top             =   2040
         Width           =   1080
      End
      Begin VB.Label lbl_Imagen_click 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Doble Click para cambiar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C0C0C0&
         Height          =   240
         Left            =   315
         TabIndex        =   48
         Top             =   2760
         Width           =   2130
      End
   End
   Begin VB.Frame FrameDatosPersonales 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   4095
      Left            =   120
      TabIndex        =   33
      Top             =   2280
      Width           =   12255
      Begin VB.TextBox txt_nacionalidad 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   480
         TabIndex        =   5
         Top             =   1680
         Width           =   2775
      End
      Begin VB.TextBox txt_codigopostal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6720
         TabIndex        =   12
         Top             =   3360
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker dp_FechaNacimiento 
         Height          =   375
         Left            =   9600
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   840
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   -2147483633
         Format          =   66387969
         CurrentDate     =   42506
      End
      Begin VB.TextBox txt_ID 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   480
         TabIndex        =   1
         Top             =   840
         Width           =   1695
      End
      Begin VB.TextBox txt_estado 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3600
         TabIndex        =   11
         Top             =   3360
         Width           =   2895
      End
      Begin VB.TextBox txt_numberhome 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3480
         TabIndex        =   6
         Top             =   1680
         Width           =   2295
      End
      Begin VB.TextBox txt_email 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   8520
         TabIndex        =   8
         Top             =   1680
         Width           =   3255
      End
      Begin VB.TextBox txt_pais 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   8640
         TabIndex        =   13
         Top             =   3360
         Width           =   3135
      End
      Begin VB.TextBox txt_Nombres 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2400
         TabIndex        =   2
         Top             =   840
         Width           =   3375
      End
      Begin VB.TextBox txt_numbermovil 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6000
         TabIndex        =   7
         Top             =   1680
         Width           =   2295
      End
      Begin VB.TextBox txt_direccion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   480
         MultiLine       =   -1  'True
         TabIndex        =   9
         Top             =   2520
         Width           =   11295
      End
      Begin VB.TextBox txt_ciudad 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   480
         TabIndex        =   10
         Top             =   3360
         Width           =   2895
      End
      Begin VB.TextBox txt_Apellidos 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6000
         TabIndex        =   3
         Top             =   840
         Width           =   3375
      End
      Begin VB.Label lbl_Pais 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   8640
         TabIndex        =   53
         Top             =   3000
         Width           =   2055
      End
      Begin VB.Label lbl_ID 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "ID"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   46
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label lbl_FechaNacimiento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Nacimiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   9600
         TabIndex        =   45
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label lbl_Estado 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   3600
         TabIndex        =   44
         Top             =   3000
         Width           =   2175
      End
      Begin VB.Label lbl_telefonoHome 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "N� Tel�fono Home"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   3480
         TabIndex        =   43
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label lbl_Email 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Correo Electr�nico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   8520
         TabIndex        =   42
         Top             =   1320
         Width           =   3135
      End
      Begin VB.Label lbl_CodigoPostal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo postal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   6720
         TabIndex        =   41
         Top             =   3000
         Width           =   1455
      End
      Begin VB.Label lbl_nacionalidad 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nacionalidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   40
         Top             =   1320
         Width           =   2895
      End
      Begin VB.Label lbl_nombres 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nombres"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   2400
         TabIndex        =   39
         Top             =   480
         Width           =   3255
      End
      Begin VB.Label lbl_telefonoMovil 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "N� Tel�fono M�vil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   6000
         TabIndex        =   38
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label lbl_Direccion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   37
         Top             =   2160
         Width           =   8415
      End
      Begin VB.Label lbl_Ciudad 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Ciudad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   36
         Top             =   3000
         Width           =   2175
      End
      Begin VB.Label lbl_apellidos 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Apellidos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   6000
         TabIndex        =   35
         Top             =   480
         Width           =   3255
      End
      Begin VB.Label lbl_DatosPersonales 
         BackStyle       =   0  'Transparent
         Caption         =   "Datos Personales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   34
         Top             =   120
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2055
         X2              =   14755
         Y1              =   300
         Y2              =   300
      End
   End
   Begin VB.Frame FrameDatosLaborales 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   1815
      Left            =   120
      TabIndex        =   26
      Top             =   6480
      Width           =   15071
      Begin VB.ComboBox cb_frecuencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         ItemData        =   "FromEmployeesAddEdit.frx":00AC
         Left            =   12600
         List            =   "FromEmployeesAddEdit.frx":00BC
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1080
         Width           =   1935
      End
      Begin MSComCtl2.DTPicker dp_FechaIngreso 
         Height          =   375
         Left            =   480
         TabIndex        =   14
         Top             =   1080
         Width           =   2160
         _ExtentX        =   3810
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   -2147483633
         Format          =   66387969
         CurrentDate     =   42506
      End
      Begin VB.TextBox txt_departamento 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         TabIndex        =   16
         Top             =   1080
         Width           =   3615
      End
      Begin VB.TextBox txt_cargo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         TabIndex        =   15
         Top             =   1080
         Width           =   3615
      End
      Begin VB.TextBox txt_salario 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   10560
         TabIndex        =   17
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2055
         X2              =   15120
         Y1              =   300
         Y2              =   300
      End
      Begin VB.Label lbl_DatosLaborales 
         BackStyle       =   0  'Transparent
         Caption         =   "Datos Laborales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   32
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label lbl_Departamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   6720
         TabIndex        =   31
         Top             =   720
         Width           =   3375
      End
      Begin VB.Label lbl_Cargo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cargo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   2880
         TabIndex        =   30
         Top             =   720
         Width           =   3375
      End
      Begin VB.Label lbl_Frecuencia 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Frecuencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   12600
         TabIndex        =   29
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label lbl_Salario 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Salario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   10560
         TabIndex        =   28
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label lbl_FechaIngreso 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Ingreso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   27
         Top             =   720
         Width           =   2295
      End
   End
   Begin VB.Frame frame_aceptar 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   13440
      MousePointer    =   99  'Custom
      TabIndex        =   20
      Top             =   1440
      Width           =   1455
      Begin VB.Label lbl_aceptar 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   0
         TabIndex        =   25
         Top             =   105
         Width           =   1455
      End
   End
   Begin VB.Frame frame_cancel 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   11520
      MousePointer    =   99  'Custom
      TabIndex        =   21
      Top             =   1440
      Width           =   1455
      Begin VB.Label lbl_cancel 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   0
         TabIndex        =   24
         Top             =   105
         Width           =   1455
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14760
         MousePointer    =   99  'Custom
         Picture         =   "FromEmployeesAddEdit.frx":00E5
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   22
         Top             =   105
         Width           =   1815
      End
   End
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   14880
      Top             =   2160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Buscar Imagenes"
   End
   Begin VB.Image logoForm 
      Appearance      =   0  'Flat
      Height          =   960
      Left            =   480
      Picture         =   "FromEmployeesAddEdit.frx":1E67
      Top             =   840
      Width           =   960
   End
   Begin VB.Label lbl_Titulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Empleados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00333333&
      Height          =   615
      Left            =   1800
      TabIndex        =   50
      Top             =   960
      Width           =   8055
   End
End
Attribute VB_Name = "FormEmployeesAddEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public isNew As Boolean
Public isPreview As Boolean
Public Codigo As String
Public imagen_name As String
Public imagen_handle As Double

Private Sub CmdTeclado_Click()
    TecladoWindows
End Sub

Private Sub Exit_Click()
    ArrResultado = Empty
    Unload Me
End Sub

Private Sub Form_Activate()
    Call AjustarPantalla(Me)
    If Not isPreview Then txt_Nombres.SetFocus
End Sub

Private Sub Form_Load()
    
    lbl_Titulo.Caption = Stellar_Mensaje(2)
    
    lbl_aceptar.Caption = Stellar_Mensaje(104)
    lbl_cancel.Caption = Stellar_Mensaje(112)
    
    lbl_DatosPersonales.Caption = Stellar_Mensaje(113)
    lbl_DatosLaborales.Caption = Stellar_Mensaje(114)
    lbl_Notas.Caption = Stellar_Mensaje(115)
    
    lbl_ID.Caption = Stellar_Mensaje(116)
    lbl_nombres.Caption = Stellar_Mensaje(117)
    lbl_apellidos.Caption = Stellar_Mensaje(118)
    lbl_FechaNacimiento.Caption = Stellar_Mensaje(119)
    lbl_nacionalidad.Caption = Stellar_Mensaje(120)
    lbl_telefonoHome.Caption = Stellar_Mensaje(121)
    lbl_telefonoMovil.Caption = Stellar_Mensaje(122)
    lbl_Email.Caption = Stellar_Mensaje(123)
    lbl_Direccion.Caption = Stellar_Mensaje(124)
    lbl_Ciudad.Caption = Stellar_Mensaje(125)
    lbl_Estado.Caption = Stellar_Mensaje(126)
    lbl_CodigoPostal.Caption = Stellar_Mensaje(127)
    lbl_Pais.Caption = Stellar_Mensaje(128)
    lbl_FechaIngreso.Caption = Stellar_Mensaje(129)
    lbl_Cargo.Caption = Stellar_Mensaje(130)
    lbl_Departamento.Caption = Stellar_Mensaje(131)
    lbl_Salario.Caption = Stellar_Mensaje(132)
    lbl_Frecuencia.Caption = Stellar_Mensaje(133)
    'lbl_Imagen_Titulo.Caption Stellar_Mensaje(134)
    lbl_Imagen_click.Caption = Stellar_Mensaje(135)
    
    cb_frecuencia.ListIndex = 0

    If isNew Then
        txt_ID.Text = GetCorrelative("Empleados")
    Else
        CargarEmpleado Codigo
    End If
    
End Sub

Private Sub Imagen_DblClick()

    On Error GoTo Errores
    
    dialogo.Filter = "Im�genes (*.jpg)|*.jpg"
   
    dialogo.ShowOpen
    
    If dialogo.FileName <> "" Then
        Imagen.Picture = LoadPicture(dialogo.FileName)
        imagen_handle = Imagen.Picture.Handle
        imagen_name = dialogo.FileName
    Else
        imagen_name = ""
    End If
    
    Exit Sub
    
Errores:
    
    MsgBox Err.Description
    Unload Me
    
End Sub

Private Sub frame_cancel_Click()
    lbl_cancel_Click
End Sub

Private Sub lbl_aceptar_Click()
    If ValidarCampos() Then
        If isNew Then
            If AgregarEmpleado() Then
                Unload Me
            End If
        Else
            If ModificarEmpleado() Then
                Unload Me
            End If
        End If
    End If
End Sub

Function PushImageToDb(PathImage As String) As Variant
    
    Dim LibreArc As Integer, Buffer As Variant
    
    Dim FS, f, s, counter As Long
    
    PushImageToDb = Null
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    STemp = PathImage
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If Not FS.FileExists(PathImage) Then
        Exit Function
    End If
    
    LibreArc = FreeFile()
    FilTam = FileLen(PathImage)
    
    Open PathImage For Binary Access Read As #LibreArc
    
    Buffer = Input(LOF(LibreArc), LibreArc)
    
    Close LibreArc
    
    PushImageToDb = Buffer
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
End Function

Private Sub lbl_cancel_Click()
    Exit_Click
End Sub

Private Sub frame_aceptar_Click()
    lbl_aceptar_Click
End Sub

Private Function ValidarCampos() As Boolean
    
    ValidarCampos = True
    
    'DATOS PERSONALES
    If txt_ID.Text = "" Then
        
        ValidarCampos = False
        txt_ID.SetFocus
        
    End If
    If txt_Nombres.Text = "" Then
        
        ValidarCampos = False
        txt_Nombres.SetFocus
        
    End If
    If txt_Apellidos.Text = "" Then
        
        ValidarCampos = False
        txt_Apellidos.SetFocus
        
    End If
'    If dp_FechaNacimiento.Text = "" Then
'
'        ValidarCampos = False
'        dp_FechaNacimiento.SetFocus
'
'    End If
'    If txt_nacionalidad.Text = "" Then
'
'        ValidarCampos = False
'        txt_nacionalidad.SetFocus
'
'    End If
'    If txt_numberhome.Text = "" Then
'
'        ValidarCampos = False
'        txt_numberhome.SetFocus
'
'    End If
'    If txt_numbermovil.Text = "" Then
'        ValidarCampos = False
'        txt_numbermovil.SetFocus
'
'    End If
'    If txt_email.Text = "" Then
'
'        ValidarCampos = False
'        txt_email.SetFocus
'
'    End If
'    If txt_direccion.Text = "" Then
'
'        ValidarCampos = False
'        txt_direccion.SetFocus
'
'    End If
'    If txt_ciudad.Text = "" Then
'
'        ValidarCampos = False
'        txt_ciudad.SetFocus
'
'    End If
'    If txt_estado.Text = "" Then
'
'        ValidarCampos = False
'        txt_estado.SetFocus
'
'    End If
'    If txt_codigopostal.Text = "" Then
'
'        ValidarCampos = False
'        txt_codigopostal.SetFocus
'
'    End If
'    If txt_pais.Text = "" Then
'
'        ValidarCampos = False
'        txt_pais.SetFocus
'
'    End If
    
    'DATOS LABORALES
'    If dp_FechaIngreso.Text = "" Then
'
'        ValidarCampos = False
'        dp_FechaIngreso.SetFocus
'
'    End If
'    If txt_cargo.Text = "" Then
'
'        ValidarCampos = False
'        txt_cargo.SetFocus
'
'    End If
'    If txt_departamento.Text = "" Then
'
'        ValidarCampos = False
'        txt_departamento.SetFocus
'
'    End If
'    If txt_salario.Text = "" Then
'
'        ValidarCampos = False
'        txt_salario.SetFocus
'
'    End If
'    If cb_frecuencia.Text = "" Then
'
'        ValidarCampos = False
'        cb_frecuencia.SetFocus
'
'    End If
    
    'NOTAS
'    If txt_notas.Text = "" Then
'
'        ValidarCampos = False
'        txt_notas.SetFocus
'
'    End If
    If Not ValidarCampos Then
        MsgBox Stellar_Mensaje(136)
    Else
        ValidarCampos = True
    End If
End Function

Private Function AgregarEmpleado() As Boolean
    
    Dim RsEmpleados As New ADODB.Recordset
    Dim sql As String
    
    On Error GoTo Errores
    
    VAD10Connection.BeginTrans
    
    Dim Image As Variant
    
    Image = ObtenerImagenFromImageControl()
    
    sql = "SELECT * FROM MA_EMPLEADOS WHERE 1 = 2"
    RsEmpleados.Open sql, VAD10Connection, adOpenKeyset, adLockOptimistic
    
    With RsEmpleados
        
        .AddNew
            !c_codigo = GetCorrelative("Empleados", True)
            !cu_nombres = txt_Nombres
            !cu_apellidos = txt_Apellidos
            !d_fechanacimiento = dp_FechaNacimiento
            !cu_nacionalidad = txt_nacionalidad
            !cu_numerohome = txt_numberhome
            !cu_numeromovil = txt_numbermovil
            !cu_email = txt_email
            !cu_direccion = txt_direccion
            !cu_ciudad = txt_ciudad
            !cu_estado = txt_estado
            !cu_codigopostal = txt_codigopostal
            !cu_pais = txt_pais
            !d_fechaingreso = dp_FechaIngreso
            !cu_cargo = txt_cargo
            !cu_departamento = txt_departamento
            !n_salario = txt_salario
            !cu_frecuencia = cb_frecuencia
            !cu_notas = txt_notas
            .Fields("Image").AppendChunk (Image)
            '!Image = Image
        .Update
        
        AgregarEmpleado = True
        
    End With
    
    VAD10Connection.CommitTrans
    
    Exit Function
    
Errores:
    
    MsgBox Err.Description
    VAD10Connection.RollbackTrans
    
End Function

Private Function ObtenerImagenFromImageControl() As Variant
    
    ObtenerImagenFromImageControl = Null
    
    Resp = Dir(imagen_name)
    
    Dim Image As Variant
    
    If (Imagen.Picture.Handle <> imagen_hanled) And imagen_name <> "" And Resp <> "" Then
        Image = PushImageToDb(imagen_name)
    Else
        If Imagen.Picture.Handle = 0 Then Image = Null
    End If
    
    ObtenerImagenFromImageControl = Image
    
End Function


Private Function ModificarEmpleado() As Boolean
    
    Dim RsEmpleados As New ADODB.Recordset
    Dim sql As String
    
    On Error GoTo Errores
    
    VAD10Connection.BeginTrans
    
    Dim Image As Variant
    
    Image = ObtenerImagenFromImageControl()
    
    sql = "SELECT * FROM MA_EMPLEADOS WHERE c_Codigo = '" & Codigo & "'"
    RsEmpleados.Open sql, VAD10Connection, adOpenKeyset, adLockOptimistic
    If Not RsEmpleados.EOF Then
        With RsEmpleados
            
            !cu_nombres = txt_Nombres
            !cu_apellidos = txt_Apellidos
            !d_fechanacimiento = FechaBD(dp_FechaNacimiento, FBD_Fecha, True)
            !cu_nacionalidad = txt_nacionalidad
            !cu_numerohome = txt_numberhome
            !cu_numeromovil = txt_numbermovil
            !cu_email = txt_email
            !cu_direccion = txt_direccion
            !cu_ciudad = txt_ciudad
            !cu_estado = txt_estado
            !cu_codigopostal = txt_codigopostal
            !cu_pais = txt_pais
            !d_fechaingreso = FechaBD(dp_FechaIngreso, FBD_Fecha, True)
            !cu_cargo = txt_cargo
            !cu_departamento = txt_departamento
            !n_salario = txt_salario
            !cu_frecuencia = cb_frecuencia.Text
            !cu_notas = txt_notas
            If Not IsEmpty(Image) Then .Fields("Image").AppendChunk (Image)
            '!Image = Image
            
            .Update
            
            ModificarEmpleado = True
            
        End With
    End If
    
    VAD10Connection.CommitTrans
    
    Exit Function
    
Errores:
    
    MsgBox Err.Description
    VAD10Connection.RollbackTrans
    'Resume ' Debug
    
End Function

Private Sub CargarEmpleado(Codigo As String)
    
    Dim rsEmpleado As New ADODB.Recordset
    Dim sql As String
    
    sql = "SELECT * FROM MA_EMPLEADOS WHERE c_Codigo = '" & Codigo & "'"
    
    rsEmpleado.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEmpleado.EOF Then
        
        With rsEmpleado
            txt_ID.Text = !c_codigo
            txt_Nombres.Text = !cu_nombres
            txt_Apellidos.Text = !cu_apellidos
            dp_FechaNacimiento = !d_fechanacimiento
            txt_nacionalidad.Text = !cu_nacionalidad
            txt_numberhome.Text = !cu_numerohome
            txt_numbermovil.Text = !cu_numeromovil
            txt_email.Text = !cu_email
            txt_direccion.Text = !cu_direccion
            txt_ciudad.Text = !cu_ciudad
            txt_estado.Text = !cu_estado
            txt_codigopostal.Text = !cu_codigopostal
            txt_pais.Text = !cu_pais
            dp_FechaIngreso = !d_fechaingreso
            txt_cargo.Text = !cu_cargo
            txt_departamento.Text = !cu_departamento
            txt_salario.Text = !n_salario
            If !cu_frecuencia <> "" Then cb_frecuencia.Text = !cu_frecuencia
            txt_notas.Text = !cu_notas
            
            If Not IsNull(!Image) Then
                Call PopImageOfDb(Imagen, rsEmpleado, "Image")
                imagen_hanled = Imagen.Picture.Handle
                
             '  Debug.Print Imagen.Width & " ," & Imagen.Height & " , " & Imagen.Picture.Height & "," & Imagen.Picture.Width
            Else
                imagen_hanled = 0
                Imagen.Picture = LoadPicture()
            End If
            
        End With
        
        If isPreview Then
            Call DesactivarCampos
        End If
        
    End If
    
End Sub

Function PopImageOfDb(ByRef ObjImg As Object, RecSet As ADODB.Recordset, Campo As Variant) As Boolean
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    
    PopImageOfDb = False
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    STemp = "STELLAR_BK001.TMP"
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If FS.FileExists(STemp) Then
        Set f = FS.GetFile(STemp)
        f.Delete
    End If
    
    LibreArc = FreeFile()
    
    Open STemp For Append Access Write As #LibreArc
    
    If Not RecSet.EOF Then
        If RecSet.Fields(Campo).ActualSize > 0 Then
            Buffer = RecSet.Fields(Campo).GetChunk(RecSet.Fields(Campo).ActualSize)
        Else
            Close LibreArc
            Exit Function
        End If
    End If
    
    Print #LibreArc, Buffer
    
    Close LibreArc
    
    ObjImg.Picture = LoadPicture(STemp)
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(STemp) Then
        Set f = FS.GetFile(STemp)
        f.Delete
    End If
    
    PopImageOfDb = True
    
End Function

Private Sub DesactivarCampos()
    txt_ID.Enabled = False
    txt_Nombres.Enabled = False
    txt_Apellidos.Enabled = False
    dp_FechaNacimiento.Enabled = False
    txt_nacionalidad.Enabled = False
    txt_numberhome.Enabled = False
    txt_numbermovil.Enabled = False
    txt_email.Enabled = False
    txt_direccion.Enabled = False
    txt_ciudad.Enabled = False
    txt_estado.Enabled = False
    txt_codigopostal.Enabled = False
    txt_pais.Enabled = False
    dp_FechaIngreso.Enabled = False
    txt_cargo.Enabled = False
    txt_departamento.Enabled = False
    txt_salario.Enabled = False
    cb_frecuencia.Enabled = False
    txt_notas.Enabled = False
    frame_aceptar.Visible = False
    frame_cancel.Visible = False
End Sub

Public Sub DesactivarEmpleado(Codigo As String)
    
    Dim sql As String
    
    sql = "UPDATE MA_EMPLEADOS SET c_Estatus = 'ANU' WHERE c_Codigo = '" & Codigo & "'"
    
    On Error GoTo Errores
    
    VAD10Connection.BeginTrans
    VAD10Connection.Execute (sql)
    VAD10Connection.CommitTrans
    
    Exit Sub
    
Errores:
    
    MsgBox Err.Description
    VAD10Connection.RollbackTrans
    
End Sub

