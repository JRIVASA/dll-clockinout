VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frm_Employees 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   10920
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10920
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7095
      Left            =   0
      TabIndex        =   0
      Top             =   3720
      Width           =   15260
      _ExtentX        =   26908
      _ExtentY        =   12515
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frm_Employees"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ArrResultado
Private mRowCell As Long, mColCell As Long, Fila As Long

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim Recordset As New ADODB.Recordset
    Recordset.Open "select * from ma_usuarios", VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not Recordset.EOF Then
        MsgBox "si hay datos"
    End If
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub grid_DblClick()
    
    If Grid.Rows <= 1 Then Exit Sub
    
    Dim TmpResult
    
    ReDim TmpResult(0 To (Grid.Cols - 1))
    
    For i = 0 To UBound(TmpResult)
        TmpResult(i) = Grid.TextMatrix(Grid.Row, i)
    Next i
    
    ArrResultado = TmpResult
    
    Unload Me
    
 End Sub

Private Sub Grid_EnterCell()
    
    On Error Resume Next
    
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        FrameInfo.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
    
        MostrarEditorTexto2 Me, Grid, CmdSelect, mRowCell, mColCell
        Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    Else
        Band = False
    End If
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pResp As Boolean = False)
    
    On Error Resume Next
    Grid.Visible = False
    With pGrd

        .RowSel = .Row
        
        If .ColWidth(4) > 10 Then
        
            If .Col <> 4 Then Band = True
            .Col = 4
            
            'CmdInfo.move .Left + .CellLeft + Fix((.CellWidth / 2) - (CmdInfo.Width / 2)), .Top + .CellTop + Fix((.CellHeight / 2) - (CmdInfo.Height / 2))
            'lblInfo.move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
            'CmdInfo.Visible = True: lblInfo.Visible = True
            'CmdInfo.ZOrder 0
            'lblInfo.ZOrder 0
            
            FrameInfo.BackColor = pGrd.BackColorSel
            'FrameInfo.BackColor = pGrd.BackColor
            FrameInfo.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
            CmdInfo.Move ((FrameInfo.Width / 2) - (CmdInfo.Width / 2)), ((FrameInfo.Height / 2) - (CmdInfo.Height / 2))
            FrameInfo.Visible = True: CmdInfo.Visible = True
            FrameInfo.ZOrder
            
        End If
        
        If .Col <> 5 Then Band = True
        .Col = 5
        
        'CmdSelect.move .Left + .CellLeft + Fix((.CellWidth / 2) - (CmdSelect.Width / 2)), .Top + .CellTop + Fix((.CellHeight / 2) - (CmdSelect.Height / 2))
        'lblSelect.move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        'CmdSelect.Visible = True: lblSelect.Visible = True
        'CmdSelect.ZOrder 0
        'lblSelect.ZOrder 0
        
        FrameSelect.BackColor = pGrd.BackColorSel
        'FrameSelect.BackColor = pGrd.BackColor
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        cellRow = .Row
        cellCol = .Col

     End With
     Grid.Visible = True
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub
