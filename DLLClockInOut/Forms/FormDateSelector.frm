VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FrmDateSelector 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000A&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7380
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12675
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7380
   ScaleWidth      =   12675
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox PicSecond 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   11475
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   25
      Top             =   3360
      Width           =   495
      Begin MSComctlLib.Slider sl_second 
         Height          =   2655
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Width           =   630
         _ExtentX        =   1111
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   6
         Max             =   59
         SelectRange     =   -1  'True
      End
   End
   Begin VB.PictureBox PicMinute 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   9900
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   23
      Top             =   3360
      Width           =   495
      Begin MSComctlLib.Slider sl_minute 
         Height          =   2655
         Left            =   0
         TabIndex        =   24
         Top             =   0
         Width           =   630
         _ExtentX        =   1111
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   6
         Max             =   59
         SelectRange     =   -1  'True
      End
   End
   Begin VB.PictureBox PicHour 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   8400
      ScaleHeight     =   2535
      ScaleWidth      =   495
      TabIndex        =   21
      Top             =   3360
      Width           =   495
      Begin MSComctlLib.Slider sl_hour 
         Height          =   2655
         Left            =   0
         TabIndex        =   22
         Top             =   0
         Width           =   630
         _ExtentX        =   1111
         _ExtentY        =   4683
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   3
         Max             =   23
         SelectRange     =   -1  'True
      End
   End
   Begin VB.Frame FrameAceptar 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   975
      Left            =   10440
      TabIndex        =   19
      Top             =   6240
      Width           =   2055
      Begin VB.Label Aceptar 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   615
         Left            =   0
         TabIndex        =   20
         Top             =   225
         Width           =   2055
      End
   End
   Begin VB.Frame Framecmd_SecondDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   11160
      TabIndex        =   17
      Top             =   2640
      Width           =   1095
      Begin VB.Label cmd_SecondDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   18
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_MinuteDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   9600
      TabIndex        =   15
      Top             =   2640
      Width           =   1095
      Begin VB.Label cmd_MinuteDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   16
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_HourDown 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   8040
      TabIndex        =   13
      Top             =   2640
      Width           =   1095
      Begin VB.Label cmd_HourDown 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   14
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_SecondUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   11160
      TabIndex        =   11
      Top             =   720
      Width           =   1095
      Begin VB.Label cmd_SecondUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_MinuteUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   9600
      TabIndex        =   9
      Top             =   720
      Width           =   1095
      Begin VB.Label cmd_MinuteUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   10
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Framecmd_HourUp 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   8040
      TabIndex        =   7
      Top             =   720
      Width           =   1095
      Begin VB.Label cmd_HourUp 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   12720
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9915
         TabIndex        =   6
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   3135
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   12120
         Picture         =   "FormDateSelector.frx":0000
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.TextBox txt_Second 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   11040
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "00"
      Top             =   1320
      Width           =   1335
   End
   Begin VB.TextBox txt_Minute 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   9480
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "00"
      Top             =   1320
      Width           =   1335
   End
   Begin VB.TextBox txt_Hour 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   7920
      Locked          =   -1  'True
      TabIndex        =   1
      Text            =   "00"
      Top             =   1320
      Width           =   1335
   End
   Begin MSComCtl2.MonthView mv_Month 
      Height          =   6660
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   7290
      _ExtentX        =   12859
      _ExtentY        =   11748
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MonthBackColor  =   16777215
      ShowToday       =   0   'False
      StartOfWeek     =   121241601
      TitleBackColor  =   11426560
      TitleForeColor  =   16777215
      TrailingForeColor=   -2147483635
      CurrentDate     =   42509
   End
End
Attribute VB_Name = "FrmDateSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Fecha As Date
Public SelectedDateType As DateTypes
Public MinDate As Date

Private FormColor As Long

Public Enum DateTypes
    Full
    JustDate
    JustHour
End Enum

Private Declare Function GetWindowLong Lib "user32" _
    Alias "GetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" _
    Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

Private Const GWL_STYLE As Long = -16
Private Const MCS_NOTODAYCIRCLE As Long = 8

Private Sub PaintSlider(pCtl, pColor As Long)
    
    'pCtl.Move -10, -10, Picture1.Width + 25
    
    pCtl.Container.BackColor = pColor
    
    SetStyle pCtl.hWnd
    SetSolidColor pCtl.hWnd, pColor
    
    SSTabSubclass pCtl.hWnd
        
    RedrawWindow pCtl.hWnd, ByVal 0&, ByVal 0&, &H1
    
End Sub

Private Sub Aceptar_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        Select Case SelectedDateType
            Case Full
                Fecha = CDate(mv_Month.Value & " " & txt_Hour & ":" & txt_Minute & ":" & txt_Second)
            Case JustDate
                Fecha = CDate(mv_Month.Value)
            Case JustHour
                Fecha = CDate(txt_Hour & ":" & txt_Minute & ":" & txt_Second)
        End Select
        If MinDate > Fecha Then
            MsgBox Stellar_Mensaje(105)
        Else
            Unload Me
        End If
    End If
End Sub

Private Sub cmd_HourUp_Click()
    If sl_hour.Value + 1 <= sl_hour.Max Then sl_hour.Value = sl_hour.Value + 1
End Sub

Private Sub cmd_HourDown_Click()
    If sl_hour.Value - 1 >= sl_hour.Min Then sl_hour.Value = sl_hour.Value - 1
End Sub

Private Sub cmd_MinuteUp_Click()
    If sl_minute.Value + 1 <= sl_minute.Max Then sl_minute.Value = sl_minute.Value + 1
End Sub

Private Sub cmd_MinuteDown_Click()
    If sl_minute.Value - 1 >= sl_minute.Min Then sl_minute.Value = sl_minute.Value - 1
End Sub

Private Sub cmd_SecondUp_Click()
    If sl_second.Value + 1 <= sl_second.Max Then sl_second.Value = sl_second.Value + 1
End Sub

Private Sub cmd_SecondDown_Click()
    If sl_second.Value - 1 >= sl_second.Min Then sl_second.Value = sl_second.Value - 1
End Sub

Private Sub Exit_Click()
    Fecha = CDbl(0)
    Unload Me
End Sub

Private Sub Form_Activate()
    mv_Month.Value = FormatDateTime(Now, vbShortDate)
    If MinDate <> CDbl(0) Then mv_Month.MinDate = FormatDateTime(MinDate, vbShortDate)
End Sub

Private Sub Form_Initialize()
    With Me.mv_Month
        SetWindowLong .hWnd, GWL_STYLE, GetWindowLong(.hWnd, GWL_STYLE) Or MCS_NOTODAYCIRCLE
    End With
End Sub

Private Sub Form_Load()

    sl_hour.Value = Hour(Now)
    sl_minute.Value = Minute(Now)
    sl_second.Value = Second(Now)
    
    FormColor = Me.BackColor
    
    PaintSlider sl_hour, FormColor
    PaintSlider sl_minute, FormColor
    PaintSlider sl_second, FormColor
    
    Aceptar.Caption = Stellar_Mensaje(104)
    
End Sub

Private Sub sl_hour_Change()
    txt_Hour.Text = Format(sl_hour.Value, "00")
End Sub

Private Sub sl_minute_Change()
    txt_Minute.Text = Format(sl_minute.Value, "00")
End Sub

Private Sub sl_second_Change()
    txt_Second.Text = Format(sl_second.Value, "00")
End Sub
