VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmBuscarProducto 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   10320
      TabIndex        =   17
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image Image2 
         Height          =   480
         Left            =   75
         Picture         =   "FrmBuscarProducto.frx":0000
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H0000C000&
      Height          =   495
      Left            =   120
      TabIndex        =   14
      Top             =   2400
      Width           =   1695
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "NUEVO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   0
         TabIndex        =   15
         Top             =   100
         Width           =   1695
      End
   End
   Begin VB.ComboBox cmbItemBusqueda 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   6360
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   2400
      Visible         =   0   'False
      Width           =   3210
   End
   Begin VB.TextBox txtDato 
      Appearance      =   0  'Flat
      BackColor       =   &H00FAFAFA&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   10320
      TabIndex        =   10
      Top             =   2400
      Width           =   4140
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   7560
      TabIndex        =   9
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   75
         Picture         =   "FrmBuscarProducto.frx":0CCA
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame FrameInfo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   5400
      TabIndex        =   8
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdInfo 
         Height          =   480
         Left            =   75
         Picture         =   "FrmBuscarProducto.frx":1994
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.PictureBox Medir 
      Height          =   375
      Left            =   12600
      ScaleHeight     =   315
      ScaleWidth      =   315
      TabIndex        =   5
      Top             =   3720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Timer Tim_Progreso 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   12600
      Top             =   4320
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   4
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14760
         Picture         =   "FrmBuscarProducto.frx":265E
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7750
      LargeChange     =   10
      Left            =   14535
      TabIndex        =   1
      Top             =   3030
      Width           =   675
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7800
      Left            =   120
      TabIndex        =   0
      Top             =   3000
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   13758
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar Barra_Prg 
      Height          =   120
      Left            =   7800
      TabIndex        =   13
      Top             =   2160
      Visible         =   0   'False
      Width           =   7380
      _ExtentX        =   13018
      _ExtentY        =   212
      _Version        =   393216
      Appearance      =   0
      Max             =   1000
      Scrolling       =   1
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Empleados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00333333&
      Height          =   615
      Left            =   1800
      TabIndex        =   16
      Top             =   960
      Width           =   2655
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   960
      Left            =   480
      Picture         =   "FrmBuscarProducto.frx":43E0
      Top             =   840
      Width           =   960
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   14520
      MouseIcon       =   "FrmBuscarProducto.frx":762A
      MousePointer    =   99  'Custom
      Picture         =   "FrmBuscarProducto.frx":7934
      Stretch         =   -1  'True
      Top             =   2355
      Width           =   600
   End
   Begin VB.Label lblTeclado 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   630
      Left            =   14400
      MousePointer    =   99  'Custom
      TabIndex        =   11
      Top             =   2355
      Width           =   855
   End
   Begin VB.Image ico_Search 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   9720
      MouseIcon       =   "FrmBuscarProducto.frx":79E0
      MousePointer    =   99  'Custom
      Picture         =   "FrmBuscarProducto.frx":9762
      Top             =   2400
      Width           =   480
   End
   Begin VB.Label lblInfo 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   7
      Top             =   6000
      Width           =   255
   End
   Begin VB.Label lblSelect 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   6
      Top             =   4680
      Width           =   255
   End
End
Attribute VB_Name = "FrmBuscarProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrCadBusCod As String
Public StrCadBusDes As String
Public StrOrderBy As String

Public ExistenciaGlobal As Boolean
Public DepositoExistencia As String
Public PrecioCliente As String
Public EvitarActivate As Boolean
Public BusquedaInstantanea As Boolean
Public CustomFontSize As String
Public Vista As FrmBuscarProducto_Vista
Public ArrResultado

Public Enum FrmBuscarProducto_Vista
    VistaPrecioCliente
    VistaMarca
End Enum

Private ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String
Private StrSQL As String
Private ArrCamposBusquedas()
Public Conex_SC As ADODB.Connection
Private Band As Boolean
Private StrSqlMasCondicion As String
Private StrCondicion As String
Private PermisoFichaProductos As Boolean

Private mRowCell As Long, mColCell As Long, Fila As Long

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Sub cmdInfo_Click()
    'MsgBox "info"
    CargarInfoProductos Grid.TextMatrix(Grid.Row, 0), PermisoFichaProductos, False, False, PermisoFichaProductos, False, False, True, False, False
End Sub

Private Sub CmdSelect_Click()
    'MsgBox "ok"
    grid_DblClick
End Sub

Private Sub CmdTeclado_Click()
    'IgnorarActivate
    'TECLADO.Show vbModal
    TecladoWindows txtDato
End Sub

Private Sub Exit_Click()
    Select Case KeyCode
        Case KeyCode
            ArrResultado = Empty
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Exit_Click
End Sub

Private Sub Form_Load()
    
    'Label1.Caption = Stellar_Mensaje(6057) 'criterios de busqueda
    'CmdBuscar(0).Caption = Stellar_Mensaje(102) 'buscar
    With Grid
        .Clear
        .SelectionMode = flexSelectionByRow
        .FixedCols = 0
        .FixedRows = 1
        .Rows = 1
        .Cols = 7
        .RowHeight(0) = 425
        .RowHeightMin = 600 '.RowHeight(0) * 2
        .FormatString = "Codigo|Nombre|Departamento|"
        .ColWidth(0) = 2000
        .ColAlignment(0) = flexAlignCenterCenter
        .ColWidth(1) = 6580
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(2) = 2000
        .ColAlignment(2) = flexAlignLeftCenter
        .ColWidth(3) = 1500
        .ColWidth(4) = 1500
        .ColWidth(5) = 1500
        .ColWidth(6) = 0 ' Columna sin datos para ocultar la seleccion de columnas en la fila de cabecero fija (0) para que no le cambie el color.
        .ScrollTrack = True
        .Row = 0
        .Col = 6
        .ColSel = 6
    End With
    
    StrSQL = "select * from ma_usuarios"
    
    ArrCamposBusquedas = Array("codusuario", "descripcion", "login_name")
    
    lblTeclado.MouseIcon = CmdTeclado.MouseIcon
    
    If Conex_SC Is Nothing Then Set Conex_SC = VAD10Connection
    'If txtDato.Text = vbNullString Then txtDato.Text = "%"

End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub grid_DblClick()
    
    If Grid.Rows <= 1 Then Exit Sub
    
    Dim TmpResult
    
    ReDim TmpResult(0 To (Grid.Cols - 1))
    
    For i = 0 To UBound(TmpResult)
        TmpResult(i) = Grid.TextMatrix(Grid.Row, i)
    Next i
    
    ArrResultado = TmpResult
    
    Unload Me
    
 End Sub

Private Sub Form_Unload(Cancel As Integer)
    EvitarActivate = False
    BusquedaInstantanea = False
    CustomFontSize = vbNullString
    PrecioCliente = vbNullString
End Sub

Private Sub Form_Activate()
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    
    Dim Longitud As Long
    
    Call AjustarPantalla(Me)
    
    'Medir.FontName = lblTitulo.FontName
    'Medir.FontSize = lblTitulo.FontSize
    'Medir.FontBold = lblTitulo.FontBold
    
    Me.lbl_Organizacion.Caption = StellarMensaje(51)
    
    'Longitud = Medir.TextWidth(lblTitulo.Caption)
    
    'If Longitud > 0 Then
        'Line1.X1 = Line1.X1 + (Longitud)
    'End If
    
    If Me.txtDato.Enabled = True Then
        Me.txtDato.SetFocus
    End If
    
    txtDato.SelStart = Len(txtDato.Text)
            
    If Trim(CustomFontSize) <> vbNullString Then Grid.Font.Size = Val(CustomFontSize)
    
    If StrOrderBy <> vbNullString Then StrOrderBy = "c_Descri"
    
    If BusquedaInstantanea Then txtDato_KeyPress vbKeyReturn
    
End Sub

Private Sub FrameInfo_Click()
    cmdInfo_Click
End Sub

Private Sub FrameSelect_Click()
    CmdSelect_Click
End Sub

Private Sub Grid_EnterCell()
    
    On Error Resume Next
    
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        FrameInfo.Visible = False
        Frame2.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
    
        MostrarEditorTexto2 Me, Grid, CmdSelect, mRowCell, mColCell
        Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pResp As Boolean = False)
    
    On Error Resume Next
    
    With pGrd

        .RowSel = .Row
        
        
        If .Col <> 3 Then Band = True
        .Col = 3
        
        FrameInfo.BackColor = pGrd.BackColorSel
        'FrameInfo.BackColor = pGrd.BackColor
        FrameInfo.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdInfo.Move ((FrameInfo.Width / 2) - (CmdInfo.Width / 2)), ((FrameInfo.Height / 2) - (CmdInfo.Height / 2))
        FrameInfo.Visible = True: CmdInfo.Visible = True
        FrameInfo.ZOrder
            
        
        If .Col <> 4 Then Band = True
        .Col = 4
        
        FrameSelect.BackColor = pGrd.BackColorSel
        'FrameSelect.BackColor = pGrd.BackColor
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        If .Col <> 5 Then Band = True
        .Col = 5
        
        Frame2.BackColor = pGrd.BackColorSel
        'FrameSelect.BackColor = pGrd.BackColor
        Frame2.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        Image2.Move ((Frame2.Width / 2) - (Image2.Width / 2)), ((Frame2.Height / 2) - (Image2.Height / 2))
        Frame2.Visible = True: Frame2.Visible = True
        Frame2.ZOrder
        
        cellRow = .Row
        cellCol = .Col

     End With
     
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub ico_Search_Click()
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub txtDato_Click()
    If ModoTouch Then CmdTeclado_Click
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Exit_Click
    End If
End Sub

Sub Desactivar_Objetos(tipo As Boolean)
    Grid.Enabled = tipo
    Grid.Visible = tipo
    'Me.CmdBuscar(0).Enabled = tipo
End Sub

Public Function Consulta_Mostrar() As Boolean
    
    Dim rsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    
    Cont = 0
    
    If Trim(StrSqlMasCondicion) = "" Then
        Exit Function
    End If
    
    Call Desactivar_Objetos(False)
    
    On Error GoTo GetError
    
    Screen.MousePointer = 11
    
    Grid.Rows = 1
    
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    If StrOrderBy <> "" Then
        StrSqlMasCondicion = StrSqlMasCondicion + " ORDER BY " + Me.StrOrderBy
    End If
    
    rsConsulta.CursorLocation = adUseServer
    
    Apertura_Recordset rsConsulta
    
    'Debug.Print StrSqlMasCondicion
    
    rsConsulta.Open StrSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly
    
    If Not rsConsulta.EOF Then
        
        rsConsulta.MoveLast
        rsConsulta.MoveFirst
        
        ContFound = 0
        
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        'Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = rsConsulta.RecordCount
        
        ContFound = rsConsulta.RecordCount
        
        i = 0
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = ContFound
        ScrollGrid.Value = 0
        
        Do Until rsConsulta.EOF
            
            DoEvents

            i = i + 1
            
            If i > Grid.Rows - 1 Then Grid.Rows = Grid.Rows + 1
                
            Grid.TextMatrix(i, 0) = isDBNull(rsConsulta!CODUSUARIO, vbNullString)
            Grid.TextMatrix(i, 1) = isDBNull(rsConsulta!DESCRIPCION, vbNullString)
            Grid.TextMatrix(i, 2) = isDBNull(rsConsulta!LOGIN_NAME, vbNullString)
            
            Me.Barra_Prg.Value = ContItems
            
            rsConsulta.MoveNext
            
            ContItems = ContItems + 1
            
        Loop
        
        If Grid.Rows > 8 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = flexScrollBarBoth
            Grid.ColWidth(1) = 5920
        Else
            Grid.ScrollBars = flexScrollBarHorizontal
            ScrollGrid.Visible = False
            Grid.ColWidth(1) = 6580
        End If
        
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        
        Call Desactivar_Objetos(True)
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
        Fila = 0
        Grid_EnterCell
        
    Else
        Grid.Col = 5
        Grid.ColSel = 5
        ScrollGrid.Visible = False
        IgnorarActivate
        MsgBox True, Stellar_Mensaje(16059) '"No hay ning�n Item que cumpla con los Par�metros de B�squeda."
    End If
    
    rsConsulta.Close
    
    Screen.MousePointer = 0
    
    Me.Barra_Prg.Value = 0
    
    On Error GoTo 0
    
    Consulta_Mostrar = True
    
    Call Desactivar_Objetos(True)
    
    Fila = 0
        
    Exit Function
    
GetError:
        
    Call Desactivar_Objetos(True)
    
    Screen.MousePointer = 0
    Me.Barra_Prg.Value = 0
    
    Call IgnorarActivate
    MsgBox Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
    Consulta_Mostrar = False
    
    If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    
End Function

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            If Trim(txtDato) = vbNullString Then
                txtDato.SetFocus
                Exit Sub
            End If
            
            If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                StrSqlMasCondicion = StrSQL
                
                Consulta_Mostrar
            Else
                
                StrSqlMasCondicion = StrSQL
                
                If (UBound(ArrCamposBusquedas) + 1) >= 1 Then
                    StrCondicion = " WHERE "
                    For Each element In ArrCamposBusquedas
                        Debug.Print element
                        StrCondicion = StrCondicion & " " & element & " like '" & txtDato.Text & "%' OR"
                    Next element
                    StrCondicion = Left(StrCondicion, Len(StrCondicion) - Len("OR"))
                    StrSqlMasCondicion = StrSQL + StrCondicion
                End If

                
                'StrCadBusDes = ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex)
                
'                If UCase(StrCadBusDes) = UCase("c_Codigo") Then
'
'                    StrCondicion = "LEFT JOIN MA_CODIGOS Codigos " & GetLines & _
'                    "ON Producto.c_Codigo = Codigos.c_CodNasa" & GetLines & _
'                    "WHERE Codigos.c_Codigo LIKE '" + Me.txtDato + "%'"
'
'                    StrSqlMasCondicion = StrSQL + StrCondicion
'
'                Else
'
'                    If InStr(UCase(StrSqlMasCondicion), "WHERE") Then
'                        StrCondicion = " AND " + StrCadBusDes + " LIKE '" + Me.txtDato + "%'"
'                    Else
'                        StrCondicion = " WHERE  " + StrCadBusDes + " LIKE '" + Me.txtDato + "%' "
'                    End If
'
'                    StrSqlMasCondicion = StrSQL + StrCondicion
'
'                End If
                
                Consulta_Mostrar
                
            End If
            
    End Select
    
    Screen.MousePointer = 0
    
    On Error GoTo 0
    
    Exit Sub
    
GetError:
    
    Call IgnorarActivate
    MsgBox Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
End Sub

Private Sub CargarInfoProductos(pCodStellar As String, Boton1 As Boolean, Boton2 As Boolean, _
Boton3 As Boolean, Boton4 As Boolean, Boton5 As Boolean, Boton6 As Boolean, _
Ver As Boolean, Proveedor As Boolean, Cod_Alt As Boolean)
    
    On Error GoTo Error_Cargar
    
    Call Apertura_Recordset(RsProductos)
    
    RsProductos.Open "SELECT * FROM MA_PRODUCTOS WHERE c_Codigo = '" & pCodStellar & "'", _
    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic
    
    Call Ficha_Productos.Cargar_Campos(RsProductos)
    Call Ficha_Productos.Habilitar_Datos(False)
    
    Ficha_Productos.Toolbar1.Buttons(1).Enabled = Boton1
    Ficha_Productos.Toolbar1.Buttons(2).Enabled = Boton2
    Ficha_Productos.Toolbar1.Buttons(6).Enabled = Boton3
    Ficha_Productos.Toolbar1.Buttons(3).Enabled = Boton4
    Ficha_Productos.Toolbar1.Buttons(10).Enabled = Boton6
    
    Ficha_Productos.Proveedor.Enabled = Proveedor
    Ficha_Productos.Cod_Alt.Enabled = Cod_Alt
    
    If Ver = True Then Ficha_Productos.Show vbModal
    
    Exit Sub
    
Error_Cargar:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)

End Sub
