VERSION 5.00
Begin VB.Form FormLogin 
   Appearance      =   0  'Flat
   BackColor       =   &H009E5300&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7185
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   11595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "FormLogin.frx":0000
   ScaleHeight     =   7185
   ScaleWidth      =   11595
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt_clave 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   6360
      PasswordChar    =   "•"
      TabIndex        =   3
      Top             =   3840
      Width           =   3495
   End
   Begin VB.TextBox txt_usuario 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   360
      Left            =   6360
      TabIndex        =   2
      Top             =   2400
      Width           =   3495
   End
   Begin VB.Image img_aceptar 
      Appearance      =   0  'Flat
      Height          =   840
      Left            =   7800
      Top             =   4950
      Width           =   840
   End
   Begin VB.Image img_cancelar 
      Appearance      =   0  'Flat
      Height          =   840
      Left            =   9000
      Top             =   4950
      Width           =   960
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   10110
      MousePointer    =   99  'Custom
      Picture         =   "FormLogin.frx":5B952
      Stretch         =   -1  'True
      Top             =   2280
      Width           =   480
   End
   Begin VB.Image Image2 
      Height          =   1500
      Left            =   3120
      Picture         =   "FormLogin.frx":5D6D4
      Top             =   8160
      Visible         =   0   'False
      Width           =   4485
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H00BC7200&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   9360
      TabIndex        =   4
      Top             =   -120
      Visible         =   0   'False
      Width           =   5295
   End
   Begin VB.Label lbl_clave 
      BackStyle       =   0  'Transparent
      Caption         =   "Contraseña"
      BeginProperty Font 
         Name            =   "Arial Unicode MS"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6360
      TabIndex        =   1
      Top             =   3360
      Width           =   1455
   End
   Begin VB.Label lbl_usuario 
      BackStyle       =   0  'Transparent
      Caption         =   "Usuario"
      BeginProperty Font 
         Name            =   "Arial Unicode MS"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   6360
      TabIndex        =   0
      Top             =   1920
      Width           =   1095
   End
End
Attribute VB_Name = "FormLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Logged As Boolean
Public Correlativo As String

Private Sub CmdTeclado_Click()
    If Me.ActiveControl Is Nothing Then Exit Sub
    TecladoWindows Me.ActiveControl
End Sub

Private Sub Form_Load()
    lbl_usuario.Caption = Stellar_Mensaje(106)
    lbl_clave.Caption = Stellar_Mensaje(107)
End Sub

Private Sub img_aceptar_Click()
    ValidarInyeccionSQL Me
    Dim rsUsuario As New ADODB.Recordset
    Dim sql As String
    sql = "select ma_usuarios.password as Clave, ma_empleados.c_codigo as CodEmpleado from ma_usuarios inner join ma_empleados on ma_empleados.c_relacion = ma_usuarios.codusuario where ma_empleados.c_relacion <> '' and ma_empleados.c_estatus = 'DCO' and ma_usuarios.login_name = '" & txt_usuario.Text & "'"
    rsUsuario.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not rsUsuario.EOF Then
        If rsUsuario!Clave = txt_clave.Text Then
            Logged = True
            Correlativo = rsUsuario!CodEmpleado
        End If
        Me.Hide
    Else
        MsgBox Stellar_Mensaje(108)
    End If
End Sub

Private Sub img_cancelar_Click()
    Unload Me
End Sub

Private Sub txt_usuario_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            txt_clave.SetFocus
    End Select
End Sub

Private Sub txt_clave_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            img_aceptar_Click
    End Select
End Sub

