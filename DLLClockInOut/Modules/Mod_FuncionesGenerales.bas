Attribute VB_Name = "Mod_FuncionesGenerales"
Public VAD10Connection As Object
Public ModoTouch As Boolean
Public Uno As Boolean
Global CampoT As Object

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" ( _
                    ByVal hWnd As Long, _
                    ByVal lpOperation As String, _
                    ByVal lpFile As String, _
                    ByVal lpParameters As String, _
                    ByVal lpDirectory As String, _
                    ByVal nShowCmd As Long) As Long

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Set Rec = Nothing
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseClient
End Sub

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            isDBNull = pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim ruta As String
    
    Dim tmpVal As Variant
    tmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(tmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
    If WindowsArchitecture = [32Bits] Then
                    
        ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(ruta) Then res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        'MsgBox ruta
        
        If PathExists(ruta) Then
            'MsgBox "existe"
            res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
            'MsgBox "Llamada Hecha"
        Else
            
            ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(ruta) Then
                'MsgBox "existe ruta 2"
                res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
                'MsgBox "llamada hecha"
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
    Exit Sub
    
End Sub

Public Function FindPath(FileName As String, FindFileInUpperLevels As FindFileConstants, Optional BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\") & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then FindPath = CurrentFilePath: Exit Function
        
        FindPath = BaseFilePath + "\" + IIf(FileName Like "*.*", FileName, FileName & "\")
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = BaseFilePath + "\" + IIf(FileName Like "*.*", FileName, FileName & "\")
        
    End If

End Function

Public Function PathExists(pPath As String) As Boolean
    On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
     If Dir(pPath) <> "" Then PathExists = True
End Function

Private Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim pos As Long
    
    pos = InStr(1, pPath, "\")
    
    If pos <> 0 Then
        GetDirectoryRoot = Left(pPath, pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim pos As Long
    
    pos = InStrRev(pPath, "\")
    
    If pos <> 0 Then
        GetDirParent = Left(pPath, pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function ShellEx(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellEx = ShellExecute(hWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

Public Function BuscarInfo_Empleado(Optional pConex, Optional pTextoABuscar, Optional isQuery As Boolean = False) As String
    
    If Not IsMissing(pConex) Then Set FrmEmployees.Conex_SC = pConex
    
    FrmEmployees.isQuery = isQuery
    
    If Not IsMissing(pTextoABuscar) Then
        FrmEmployees.BusquedaInstantanea = True
        FrmEmployees.txtDato.Text = pTextoABuscar
    End If
    
    FrmEmployees.Show vbModal
    
    If isQuery Then BuscarInfo_Empleado = FrmEmployees.CodigoRetorno
    
    Set FrmEmployees = Nothing
    
End Function

Public Function GetCorrelative(Campo As String, Optional Avanzar As Boolean = False)
    Dim rsCorrelativo As New ADODB.Recordset
    Dim sql As String
    sql = "select * from ma_correlativos where CU_Campo='" & Campo & "'"
    rsCorrelativo.Open sql, VAD10Connection, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not rsCorrelativo.EOF Then
        Dim Correlativo As String
        Correlativo = Format(rsCorrelativo!nu_valor + 1, rsCorrelativo!cu_formato)
        If Avanzar Then
            VAD10Connection.Execute ("update ma_correlativos set nu_valor = nu_valor + 1 where cu_campo = '" & Campo & "'")
        End If
        GetCorrelative = Correlativo
    End If
End Function

Public Function ValidarInyeccionSQL(Parent As Form) As Boolean
    For Each Child In Parent.Controls
        If TypeOf Child Is TextBox Then
            Child.Text = Replace(Child.Text, "'", "")
        End If
    Next Child
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function Interfaz_SeleccionDeFecha(TipoFecha As DateTypes, Optional MinDate As Date = CDate(0)) As Date
    
    FrmDateSelector.SelectedDateType = TipoFecha
    If CDbl(MinDate) <> 0 Then FrmDateSelector.MinDate = MinDate
    
    FrmDateSelector.Show vbModal
    
    Interfaz_SeleccionDeFecha = FrmDateSelector.Fecha
    Set FrmDateSelector = Nothing
    
End Function

