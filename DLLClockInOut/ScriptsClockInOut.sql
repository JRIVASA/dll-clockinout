DECLARE @TmpQuery NVARCHAR(MAX)

USE [VAD10]

/****** Object:  Table [dbo].[MA_EMPLEADOS]    Script Date: 5/23/2016 4:20:54 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MA_EMPLEADOS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MA_EMPLEADOS](
	[c_Codigo] [nvarchar](20) NOT NULL,
	[c_Relacion] [nvarchar](20) NOT NULL,
	[cu_Nombres] [nvarchar](250) NOT NULL,
	[cu_Apellidos] [nvarchar](250) NOT NULL,
	[d_FechaNacimiento] [date] NOT NULL,
	[cu_Nacionalidad] [nvarchar](50) NOT NULL,
	[cu_NumeroHome] [nvarchar](50) NOT NULL,
	[cu_NumeroMovil] [nvarchar](50) NOT NULL,
	[cu_Email] [nvarchar](50) NOT NULL,
	[cu_Direccion] [nvarchar](max) NOT NULL,
	[cu_Ciudad] [nvarchar](50) NOT NULL,
	[cu_Estado] [nvarchar](50) NOT NULL,
	[cu_CodigoPostal] [nvarchar](50) NOT NULL,
	[cu_Pais] [nvarchar](50) NOT NULL,
	[d_FechaIngreso] [date] NOT NULL,
	[cu_Cargo] [nvarchar](50) NOT NULL,
	[cu_Departamento] [nvarchar](50) NOT NULL,
	[n_Salario] [float] NOT NULL,
	[cu_Frecuencia] [nvarchar](50) NOT NULL,
	[cu_Notas] [nvarchar](50) NOT NULL,
	[c_Estatus] [nvarchar](3) NOT NULL,
	[MarcajeEstatus] [tinyint] NOT NULL,
	[Image] [image] NULL,
 CONSTRAINT [PK_MA_EMPLEADOS] PRIMARY KEY CLUSTERED 
(
	[c_codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

/****** Object:  Table [dbo].[TR_EMPLEADOS_MARCAJES]    Script Date: 5/23/2016 4:20:54 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TR_EMPLEADOS_MARCAJES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TR_EMPLEADOS_MARCAJES](
	[IdMarcaje] [nvarchar](20) NOT NULL,
	[CodigoEmpleado] [nvarchar](20) NOT NULL,
	[ClockInDate] [datetime] NOT NULL,
	[ClockOutDate] [datetime] NULL,
	[Horas] [int] NOT NULL,
	[Minutos] [int] NOT NULL,
 CONSTRAINT [PK_TR_Empleados_Marcajes] PRIMARY KEY CLUSTERED 
(
	[IdMarcaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_c_Relacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_c_Relacion]  DEFAULT ('') FOR [c_Relacion]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Nombres]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Nombres]  DEFAULT ('') FOR [cu_Nombres]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Apellidos]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Apellidos]  DEFAULT ('') FOR [cu_Apellidos]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_d_FechaNacimiento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_d_FechaNacimiento]  DEFAULT (getdate()) FOR [d_FechaNacimiento]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Nacionalidad]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Nacionalidad]  DEFAULT ('') FOR [cu_Nacionalidad]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_NumeroHome]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_NumeroHome]  DEFAULT ('') FOR [cu_NumeroHome]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_NumeroMovil]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_NumeroMovil]  DEFAULT ('') FOR [cu_NumeroMovil]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Email]  DEFAULT ('') FOR [cu_Email]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Direccion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Direccion]  DEFAULT ('') FOR [cu_Direccion]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Ciudad]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Ciudad]  DEFAULT ('') FOR [cu_Ciudad]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Estado]  DEFAULT ('') FOR [cu_Estado]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_CodigoPostal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_CodigoPostal]  DEFAULT ('') FOR [cu_CodigoPostal]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Pais]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Pais]  DEFAULT ('') FOR [cu_Pais]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_d_FechaIngreso]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_d_FechaIngreso]  DEFAULT (getdate()) FOR [d_FechaIngreso]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Cargo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Cargo]  DEFAULT ('') FOR [cu_Cargo]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Departamento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Departamento]  DEFAULT ('') FOR [cu_Departamento]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_n_Salario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_n_Salario]  DEFAULT ((0)) FOR [n_Salario]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Frecuencia]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Frecuencia]  DEFAULT ('') FOR [cu_Frecuencia]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_cu_Notas]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_cu_Notas]  DEFAULT ('') FOR [cu_Notas]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_c_Estatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_c_Estatus]  DEFAULT ('DCO') FOR [c_Estatus]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MA_EMPLEADOS_MarcajeEstatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MA_EMPLEADOS] ADD  CONSTRAINT [DF_MA_EMPLEADOS_MarcajeEstatus]  DEFAULT ((0)) FOR [MarcajeEstatus]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TR_EMPLEADOS_MARCAJES_ClockInDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TR_EMPLEADOS_MARCAJES] ADD  CONSTRAINT [DF_TR_EMPLEADOS_MARCAJES_ClockInDate]  DEFAULT (getdate()) FOR [ClockInDate]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TR_Empleados_Marcajes_Horas]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TR_EMPLEADOS_MARCAJES] ADD  CONSTRAINT [DF_TR_Empleados_Marcajes_Horas]  DEFAULT ((0)) FOR [Horas]
END

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TR_Empleados_Marcajes_Minutos]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TR_EMPLEADOS_MARCAJES] ADD  CONSTRAINT [DF_TR_Empleados_Marcajes_Minutos]  DEFAULT ((0)) FOR [Minutos]
END

------

USE VAD10

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'admin')
BEGIN
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modclo')
	BEGIN
		INSERT INTO ESTRUC_MENU (Relacion, tiporel, Clave, texto, imagen, tag, pos)
		VALUES ('admin', 'tvwchild', 'modclo', 'Clock In / Out', 'ClockInOut', 'modclo', 0)
	
		Print 'Opción de Menú agregada con exito.'
	END
	ELSE
	BEGIN
		UPDATE ESTRUC_MENU SET
		Relacion = 'admin',
		tiporel = 'tvwchild',
		texto = 'Clock In / Out',
		imagen = 'ClockInOut',
		tag = 'modclo',
		pos = 0
		WHERE Clave = 'modclo'
	
		Print 'Opción de Menú actualizada con exito.'
	END

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''395'' WHERE Clave = ''modclo''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''395'' WHERE Clave_Menu = ''modclo''
		'
		EXEC (@TmpQuery)
	END
END	
ELSE
BEGIN
	PRINT 'No se agregó la opción de menú debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opción de menú Padre para' + CHAR(13) +
	'mantener la jerarquía Y evitar daños en el sistema. No se realizó ningún cambio.'
END

------

USE VAD10

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modclo')
BEGIN
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'FichaClockInOutUsuarios')
	BEGIN
		INSERT INTO ESTRUC_MENU (Relacion, tiporel, Clave, texto, imagen, tag, pos)
		VALUES ('modclo', 'tvwchild', 'FichaClockInOutUsuarios', 'User Access', 'ClockInOutUser', 'FichaClockInOutUsuarios', 0)
	
		Print 'Opción de Menú agregada con exito.'
	END
	ELSE
	BEGIN
		UPDATE ESTRUC_MENU SET
		Relacion = 'modclo',
		tiporel = 'tvwchild',
		texto = 'User Access',
		imagen = 'ClockInOutUser',
		tag = 'FichaClockInOutUsuarios',
		pos = 0
		WHERE Clave = 'FichaClockInOutUsuarios'
	
		Print 'Opción de Menú actualizada con exito.'
	END

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''396'' WHERE Clave = ''FichaClockInOutUsuarios''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''396'' WHERE Clave_Menu = ''FichaClockInOutUsuarios''
		'
		EXEC (@TmpQuery)
	END
END	
ELSE
BEGIN
	PRINT 'No se agregó la opción de menú debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opción de menú Padre para' + CHAR(13) +
	'mantener la jerarquía Y evitar daños en el sistema. No se realizó ningún cambio.'
END

------

USE VAD10

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modclo')
BEGIN
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'FichaClockInOutSupervisor')
	BEGIN
		INSERT INTO ESTRUC_MENU (Relacion, tiporel, Clave, texto, imagen, tag, pos)
		VALUES ('modclo', 'tvwchild', 'FichaClockInOutSupervisor', 'Supervisor Access', 'ClockInOutSuper', 'FichaClockInOutSupervisor', 0)
	
		Print 'Opción de Menú agregada con exito.'
	END
	ELSE
	BEGIN
		UPDATE ESTRUC_MENU SET
		Relacion = 'modclo',
		tiporel = 'tvwchild',
		texto = 'Supervisor Access',
		imagen = 'ClockInOutSuper',
		tag = 'FichaClockInOutSupervisor',
		pos = 0
		WHERE Clave = 'FichaClockInOutSupervisor'
	
		Print 'Opción de Menú actualizada con exito.'
	END

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''397'' WHERE Clave = ''FichaClockInOutSupervisor''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''397'' WHERE Clave_Menu = ''FichaClockInOutSupervisor''
		'
		EXEC (@TmpQuery)
	END
END	
ELSE
BEGIN
	PRINT 'No se agregó la opción de menú debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opción de menú Padre para' + CHAR(13) +
	'mantener la jerarquía Y evitar daños en el sistema. No se realizó ningún cambio.'
END

-----

USE VAD10

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modclo')
BEGIN
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'FichaEmpleados')
	BEGIN
		INSERT INTO ESTRUC_MENU (Relacion, tiporel, Clave, texto, imagen, tag, pos)
		VALUES ('modclo', 'tvwchild', 'FichaEmpleados', 'Employees Tab', 'ClockInOutEmp', 'FichaEmpleados', 0)
	
		Print 'Opción de Menú agregada con exito.'
	END
	ELSE
	BEGIN
		UPDATE ESTRUC_MENU SET
		Relacion = 'modclo',
		tiporel = 'tvwchild',
		texto = 'Employees Tab',
		imagen = 'ClockInOutEmp',
		tag = 'FichaEmpleados',
		pos = 0
		WHERE Clave = 'FichaEmpleados'
	
		Print 'Opción de Menú actualizada con exito.'
	END

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''398'' WHERE Clave = ''FichaEmpleados''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''398'' WHERE Clave_Menu = ''FichaEmpleados''
		'
		EXEC (@TmpQuery)
	END
END	
ELSE
BEGIN
	PRINT 'No se agregó la opción de menú debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opción de menú Padre para' + CHAR(13) +
	'mantener la jerarquía Y evitar daños en el sistema. No se realizó ningún cambio.'
END

-----

USE VAD10

IF NOT EXISTS(SELECT cu_Campo FROM MA_CORRELATIVOS WHERE cu_Campo = 'Empleados')
BEGIN
	INSERT INTO MA_CORRELATIVOS (cu_Campo, cu_Descripcion, nu_Valor, cu_Formato) 
	VALUES ('Empleados', 'Correlativo de MA_EMPLEADOS', 0, '000000000')
	PRINT 'Correlativo ingresado con exito.'
END
ELSE
BEGIN
	--UPDATE MA_CORRELATIVOS SET 
	--NU_VALOR = VALORNUMERICO
	--WHERE CU_CAMPO = 'CORRELATIVO_A_BUSCAR'
	PRINT 'El Correlativo ya existe. Verifique su valor.'
END

-----

USE VAD10

IF NOT EXISTS(SELECT cu_Campo FROM MA_CORRELATIVOS WHERE cu_Campo = 'EMPLEADOS_MARCAJES')
BEGIN
	INSERT INTO MA_CORRELATIVOS (cu_Campo, cu_Descripcion, nu_Valor, cu_Formato) 
	VALUES ('EMPLEADOS_MARCAJES', 'CORRELATIVO DE MARCAJES DE EMPLEADOS', 0, '000000000')
	PRINT 'Correlativo ingresado con exito.'
END
ELSE
BEGIN
	--UPDATE MA_CORRELATIVOS SET 
	--NU_VALOR = VALORNUMERICO
	--WHERE CU_CAMPO = 'CORRELATIVO_A_BUSCAR'
	PRINT 'El Correlativo ya existe. Verifique su valor.'
END
