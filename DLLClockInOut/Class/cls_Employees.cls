VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Employees"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Sub OpenForm(AdmConnection As Object, ModoTouch As Boolean)
    Set VAD10Connection = AdmConnection
    Mod_FuncionesGenerales.ModoTouch = ModoTouch
    'BuscarInfo_Empleado VAD10Connection, ""
    FormClockInOut.Show vbModal
End Sub

Public Sub OpenFormEmployees(AdmConnection As Object, ModoTouch As Boolean)
    Set VAD10Connection = AdmConnection
    Mod_FuncionesGenerales.ModoTouch = ModoTouch
    BuscarInfo_Empleado VAD10Connection, ""
End Sub

Public Sub ClockInOutUser(AdmConnection As Object)
    Set VAD10Connection = AdmConnection
    FormLogin.Show vbModal
    Dim Correlativo As String
    Dim Relacion As String
    If FormLogin.Logged Then
        Correlativo = FormLogin.Correlativo
    End If
    Unload FormLogin
    Set FormLogin = Nothing
    If Correlativo <> "" Then
        FormClockInOutSelecter.Correlativo = Correlativo
        FormClockInOutSelecter.Show vbModal
        Set FormClockInOutSelecter = Nothing
    End If
End Sub

Public Sub ClockInOutSupervisor(AdmConnection As Object)
    Set VAD10Connection = AdmConnection
    FormClockInOutSelecter.isSupervisor = True
    FormClockInOutSelecter.Show vbModal
    Set FormClockInOutSelecter = Nothing
End Sub
